package com.example.netpoll.ui.adapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.netpoll.R
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.login.LoginActivity
import java.util.*
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.locks.ReentrantLock

class FeedSurveyAdapter(private val surveyList: ArrayList<SurveyModel>,
                        private val context: Context):

    RecyclerView.Adapter<FeedSurveyAdapter.ViewHolder>()
{

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.survey_item_feed, parent, false)
        return ViewHolder((view))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val survey = surveyList[position]
        holder.profilePicture.setImageBitmap(Utilities.convertStringToBitmap(survey.profilePicture))
        holder.question.text = survey.question
        holder.username.text = survey.username
        setAnswers(holder, survey)
        holder.picture.setOnClickListener {
            showDialog(context, survey.picture)
        }

    }

    override fun getItemCount(): Int {
        return this.surveyList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun setAnswers(holder: ViewHolder, survey: SurveyModel){
        if (holder.answers.childCount == 0) {
            val table = TableLayout(context)
            val checkboxes = ArrayList<CheckBoxCell>()
            for (answer in survey.answers!!.keys) {
                val row = TableRow(context)
                val answerCell = TextViewCell(context, "$answer   ")
                val boxToChecked = if (LoginActivity.auth.uid!! in survey.peoplesAnswers.keys) survey.peoplesAnswers[LoginActivity.auth.uid!!] else ""
                val checkbox = CheckBoxCell(context, boxToChecked == answer, survey.question, answer)
                checkboxes.add(checkbox)
                row.addView(answerCell)
                row.addView(checkbox)
                table.addView(row)
            }
            for (checkbox in checkboxes)
                checkbox.setOnClickListener {
                    setUserAnswerInUi(checkboxes, checkbox.isChecked, checkbox.tag.toString())
                }
            holder.answers.addView(table)
        }
    }

    private fun setUserAnswerInUi(checkboxes: ArrayList<CheckBoxCell>, isChecked: Boolean, tag: String){
        var answerChecked = ""
        var answerUnChecked = ""
        var action = 1
        var question = ""
        for (checkbox in checkboxes) {
            if (checkbox.tag.toString() == tag) {
                action = if (isChecked) 1 else -1
                question = tag.split("@").first()
                answerChecked = tag.split("@").last()

            }
            else {
                if (checkbox.isChecked) {
                    answerUnChecked = checkbox.tag.toString().split("@").last()
                }
                checkbox.isChecked = false
            }
        }

        FirebaseStore.updateSurveyAnswer(question, answerChecked, answerUnChecked, action)

    }

    private fun showDialog(context: Context, picture: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.survey_picture_dialog)
        val surveyPicture = dialog.findViewById(R.id.iv_dialog_survey_picture) as ImageView
        val surveyCancel = dialog.findViewById(R.id.iv_dialog_survey_cancel) as ImageView
        surveyPicture.setImageBitmap(Utilities.convertStringToBitmap(picture))
        surveyCancel.setOnClickListener {
            dialog.dismiss()
        }
        if (picture != "")
            dialog.show()
    }


    @SuppressLint("ViewConstructor")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    class TextViewCell(context: Context, text: String): androidx.appcompat.widget.AppCompatTextView(context){
        init {
            this.text = text
            this.background = context.getDrawable(R.drawable.cell)
            this.width = context.resources.getDimension(R.dimen.feed_answer).toInt()
            this.isVerticalScrollBarEnabled = true
            this.setTypeface(Typeface.DEFAULT_BOLD)
        }
    }

    @SuppressLint("ViewConstructor")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    class CheckBoxCell(context: Context, isChecked: Boolean, question: String, answer: String): androidx.appcompat.widget.AppCompatCheckBox(context) {
        init {
            this.isChecked = isChecked
            this.tag = "$question@$answer"
            this.buttonTintList = context.resources.getColorStateList(R.color.primaryDarkColor)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profilePicture: ImageView = itemView.findViewById(R.id.iv_feed_survey_profile_picture)
        var username : TextView = itemView.findViewById(R.id.tv_feed_username)
        var question: TextView = itemView.findViewById(R.id.tv_feed_survey_question)
        var answers : LinearLayout = itemView.findViewById(R.id.ll_feed_answers)
        var picture: ImageView = itemView.findViewById(R.id.iv_feed_survey_picture)
    }
}