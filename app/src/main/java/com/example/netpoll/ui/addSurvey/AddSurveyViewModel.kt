package com.example.netpoll.ui.addSurvey

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.ui.addSurvey.answer.Answer
import java.util.*

class AddSurveyViewModel: ViewModel() {
     val question = MutableLiveData<String>("")
     val answers = MutableLiveData<MutableList<Answer>>(ArrayList<Answer>())
     val surveyPic = MutableLiveData<String>("")
     val answerNumber = MutableLiveData<Int>(1)
}