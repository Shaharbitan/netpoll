package com.example.netpoll.ui.adapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.ui.profile.ProfileFragmentDirections
import java.util.*


class ProfileSurveyAdapter(
    private val surveyList: ArrayList<SurveyModel>,
    private val context: Context
):
    RecyclerView.Adapter<ProfileSurveyAdapter.ViewHolder>() {

    companion object {
        var pictureAction = ""
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.survey_item, parent, false)
        return ViewHolder((view))

    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val survey = surveyList[position]
        holder.profilePic.setImageBitmap(Utilities.convertStringToBitmap(survey.profilePicture))
        holder.question.text = survey.question
        setAnswers(holder, survey)
        holder.surveyPicture.setOnClickListener {
            showDialog(context, survey.picture)
        }
        holder.surveyEdit.setOnClickListener {
            safeNavToEditSurveyFragment(survey.question, survey.picture)
        }
    }

    override fun getItemCount(): Int {
        return this.surveyList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun setAnswers(holder: ViewHolder, survey: SurveyModel){
        if (holder.answers.childCount == 0) {
            val table = TableLayout(context)
            for (answer in survey.answers!!.keys) {
                val row = TableRow(context)
                val answerCell = Cell(context, "$answer   ", true)
                val valueCell = Cell(context, " " + survey.answers!![answer].toString() + " ", false)
                row.addView(answerCell)
                row.addView(valueCell)
                table.addView(row)
            }
            holder.answers.addView(table)
        }
    }

    private fun showDialog(context: Context, picture: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.survey_picture_dialog)
        val surveyPicture = dialog.findViewById(R.id.iv_dialog_survey_picture) as ImageView
        val surveyCancel = dialog.findViewById(R.id.iv_dialog_survey_cancel) as ImageView
        surveyPicture.setImageBitmap(Utilities.convertStringToBitmap(picture))
        surveyCancel.setOnClickListener {
            dialog.dismiss()
        }
        if (picture != "")
            dialog.show()
    }

    private fun safeNavToEditSurveyFragment(question: String, picture: String){
        pictureAction = picture
        val action = ProfileFragmentDirections.actionProfileFragmentToEditSurveyFragment(question)
        MainActivity.navController.navigate(action)
    }

    @SuppressLint("ViewConstructor")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    class Cell(context: Context, text: String, isAnswer: Boolean): androidx.appcompat.widget.AppCompatTextView(context){
        init {
            this.text = text
            this.setTypeface(Typeface.DEFAULT_BOLD)
            if (isAnswer) {
                this.background = context.getDrawable(R.drawable.cell)
                this.width = context.resources.getDimension(R.dimen.answer).toInt()
                this.isVerticalScrollBarEnabled = true
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profilePic: ImageView = itemView.findViewById<View>(R.id.iv_survey_profile_picture) as ImageView
        var question: TextView = itemView.findViewById<View>(R.id.tv_survey_question) as TextView
        var answers : LinearLayout = itemView.findViewById(R.id.answers)
        var surveyPicture : ImageView = itemView.findViewById(R.id.iv_survey_picture)
        var surveyEdit : ImageView = itemView.findViewById(R.id.iv_survey_edit)
    }

}