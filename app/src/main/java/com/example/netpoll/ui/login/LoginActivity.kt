package com.example.netpoll.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.netpoll.MainActivity
import com.example.netpoll.Utilities.Extensions.validate
import com.example.netpoll.databinding.ActivityLoginBinding
import com.example.netpoll.modules.room.RoomManager
import com.example.netpoll.ui.signUp.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLoginBinding

    companion object {
        val auth: FirebaseAuth = FirebaseAuth.getInstance()
        lateinit var roomDb : RoomManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        roomDb = RoomManager(this)
        checkIfAlreadyLoggedIn()
        setContentView(binding.root)
        setUpListeners()
    }

    private fun setUpListeners(){
        binding.btnLoginLogin.setOnClickListener {
            login()
        }
        binding.btnLoginSignup.setOnClickListener{
            navigateToSignUpActivity()
        }
    }

    private fun login() {
        val email = et_login_email.text.toString()
        val password = et_login_password.text.toString()
        if (email.validate() && password.validate()) {
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) {
                if (it.isSuccessful) {
                    navigateToMainActivity()
                } else
                    Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        else {
            Toast.makeText(this, "please enter valid email and password", Toast.LENGTH_SHORT).show()
        }
    }

    private fun navigateToMainActivity(){
        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }

    private fun navigateToSignUpActivity(){
        startActivity(Intent(applicationContext, SignUpActivity::class.java))
        finish()
    }

    private fun checkIfAlreadyLoggedIn(){
        if (auth.currentUser != null){
            navigateToMainActivity()
        }
    }
}