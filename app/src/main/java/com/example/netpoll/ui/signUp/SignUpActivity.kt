package com.example.netpoll.ui.signUp

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.provider.MediaStore
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.Utilities.Extensions.validate
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.databinding.ActivitySignUpBinding
import com.example.netpoll.ui.login.LoginActivity
import com.example.netpoll.models.ProfileModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore

class SignUpActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySignUpBinding
    private var profilePic: String = ""
    private lateinit var loadingDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpListeners()
    }

    private fun setUpListeners(){
        binding.btnSignupSignup.setOnClickListener {
            signUpUser()
        }
        binding.btnSignupAlreadyHaveAccount.setOnClickListener {
            navigateToLogin()
        }
        binding.ivSignupProfilePic.setOnClickListener{
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, 100)
        }
    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 100) {
            val profilePicUri = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, profilePicUri)
            profilePic = Utilities.convertBitmapToString(bitmap)
            binding.ivSignupProfilePic.setImageURI(profilePicUri)
        }
    }

    private fun signUpUser() {
        val userName = binding.etSignupUsername.text.toString()
        val email = binding.etSignupEmail.text.toString()
        val pass = binding.etSignupPassword.text.toString()
        if (email.validate() && pass.validate() && profilePic.validate()) {
            LoginActivity.auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
                if (it.isSuccessful) {
                    showLoadingDialog()
                    val profile = ProfileModel(userName, LoginActivity.auth.currentUser!!.uid, email, pass, profilePic)
                    FirebaseStore.addProfile(profile, this)
                } else {
                    Toast.makeText(this, Utilities.getException(it.exception.toString()), Toast.LENGTH_SHORT).show()
                }
            }
        }
        else {
            Toast.makeText(this, "please enter validate username ,password and profile picture", Toast.LENGTH_SHORT).show()

        }
    }

     private fun showLoadingDialog() {
        loadingDialog = Dialog(this)
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setCancelable(false)
        loadingDialog.setContentView(R.layout.loading_dialog)
        loadingDialog.show()
    }

    fun signUpSuccess(profile: ProfileModel){
        loadingDialog.dismiss()
        LoginActivity.roomDb.saveProfile(profile)
        navigateToMainActivity()
    }

    fun signUpFailed(error: String){
        loadingDialog.dismiss()
        LoginActivity.auth.currentUser?.delete()
        Toast.makeText(this, Utilities.getException(error), Toast.LENGTH_SHORT).show()
    }

    private fun navigateToLogin(){
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun navigateToMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}