package com.example.netpoll.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel: ViewModel() {
    val profilePic = MutableLiveData<String>("")
    val username = MutableLiveData<String>("")
}