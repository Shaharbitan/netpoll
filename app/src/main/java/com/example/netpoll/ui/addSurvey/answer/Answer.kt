package com.example.netpoll.ui.addSurvey.answer

import android.content.Context
import android.text.InputType
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.netpoll.R

class Answer(context: Context, id: Int, width: Int): androidx.appcompat.widget.AppCompatEditText(context) {
    private val layoutParams = LinearLayout.LayoutParams(width - 50, 150)
    init {
        this.background = ContextCompat.getDrawable(context, R.drawable.et)
        this.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
        this.hint = "answer $id..."
        this.id = id
        this.tag = id
        this.layoutParams.setMargins(20,0,20,20)
    }

    override fun getLayoutParams(): LinearLayout.LayoutParams {
        return this.layoutParams
    }
}