package com.example.netpoll.ui.addSurvey

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.util.Pair
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.Utilities.Extensions.toHashMap
import com.example.netpoll.Utilities.Extensions.validate
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.databinding.FragmentAddSurveyBinding
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.modules.spellChecker.SpellChecker
import com.example.netpoll.modules.spellChecker.SpellCheckerListener
import com.example.netpoll.ui.addSurvey.answer.Answer
import com.example.netpoll.ui.login.LoginActivity
import java.text.ParseException
import java.util.*


class AddSurveyFragment : Fragment(), SpellCheckerListener {
    private lateinit var binding: FragmentAddSurveyBinding
    private val displayMetrics = DisplayMetrics()
    private lateinit var locationManager: LocationManager
    private lateinit var loadingDialog: Dialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_survey, container, false);
        val addSurveyViewModel = ViewModelProvider(requireActivity()).get(AddSurveyViewModel::class.java)
        binding.addSurveyViewModel = addSurveyViewModel
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        checkConnection()
        setUpUi()
        setUpListeners()
        setUpObservers()
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == 100) {
            val profilePicUri = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(
                requireContext().contentResolver,
                profilePicUri
            )
            setUpUi()
            binding.addSurveyViewModel!!.surveyPic.value = Utilities.convertBitmapToString(bitmap)
            binding.ivAddAddimage.setImageURI(profilePicUri)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        saveState()
    }

    override fun onStop() {
        super.onStop()
        saveState()
    }

    private fun setUpObservers(){
        binding.addSurveyViewModel!!.answerNumber.observe(viewLifecycleOwner) {
            binding.ivAddRemove.visibility = if (binding.addSurveyViewModel!!.answerNumber.value!! > 3) View.VISIBLE else View.INVISIBLE
        }
    }

    private fun setUpListeners(){
        binding.ivAddAdd.setOnClickListener {
            addAnswer()
        }
        binding.ivAddAddimage.setOnClickListener {
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, 100)
        }
        binding.ivAddRemove.setOnClickListener {
            removeAnswer()
        }
        binding.ivAddSave.setOnClickListener {
            saveSurvey()
        }
        binding.tvSpellChecker.setOnClickListener{
            checkSpell()
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setUpUi(){
        binding.tvSpellChecker.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        binding.etAddQuestion.setText(binding.addSurveyViewModel!!.question.value)
        setUpSurveyPic()
        setUpAnswers()
    }

    private fun addAnswer(){
        val newAnswer = Answer(
            requireContext(),
            binding.addSurveyViewModel!!.answerNumber.value!!,
            displayMetrics.widthPixels
        )
        binding.clAnswers.addView(newAnswer, newAnswer.layoutParams)
        binding.addSurveyViewModel!!.answerNumber.value = binding.addSurveyViewModel!!.answerNumber.value!! + 1
        if (binding.clAnswers.childCount > 2){
            binding.ivAddRemove.visibility = View.VISIBLE
        }
    }


    private fun removeAnswer(){
        binding.clAnswers.removeViewAt(binding.clAnswers.childCount - 1)
        binding.addSurveyViewModel!!.answerNumber.value = binding.addSurveyViewModel!!.answerNumber.value!! - 1
        if (binding.clAnswers.childCount < 3) {
            binding.ivAddRemove.visibility = View.INVISIBLE }
    }

    private fun clearState(){
        binding.addSurveyViewModel!!.question.value = ""
        binding.addSurveyViewModel!!.surveyPic.value = ""
        binding.addSurveyViewModel!!.answerNumber.value = 1
        binding.addSurveyViewModel!!.answers.value?.clear()
    }

    private fun saveState(){
        binding.addSurveyViewModel!!.question.value = binding.etAddQuestion.text.toString()
        binding.addSurveyViewModel!!.answers.value?.clear()
        for (answer in binding.clAnswers.children){
            binding.addSurveyViewModel!!.answers.value?.add(answer as Answer)
        }
        binding.clAnswers.removeAllViews()
    }

    private fun setUpSurveyPic(){
        val surveyPicture = binding.addSurveyViewModel!!.surveyPic.value!!
        if (surveyPicture == "")
            binding.ivAddAddimage.setImageDrawable(requireContext().getDrawable(R.drawable.ic_image))
        else
            binding.ivAddAddimage.setImageBitmap(Utilities.convertStringToBitmap(surveyPicture))
    }

    private fun setUpAnswers(){
        val answers = binding.addSurveyViewModel!!.answers.value
        if (answers != null && answers.size > 0) {
            binding.addSurveyViewModel!!.answerNumber.postValue(answers.size + 1)
            for (answer in answers) {
                binding.clAnswers.addView(answer, answer.layoutParams)
            }
        }
        else {
            addAnswer()
            addAnswer()
        }
    }

    private fun saveSurvey(){
        saveState()
        val location = gettingLocation()
        val survey = SurveyModel(
            LoginActivity.auth.currentUser!!.uid,
            "",
            "",
            binding.addSurveyViewModel!!.question.value!!,
            binding.addSurveyViewModel!!.answers.value!!.toHashMap(),
            binding.addSurveyViewModel!!.surveyPic.value!!,
            HashMap<String, String>(), location.first, location.second)
        if (survey.validate()) {
            showLoadingDialog()
            FirebaseStore.addSurvey(survey, this)
        }
        else
            Toast.makeText(requireContext(), "the answers or question empty", Toast.LENGTH_SHORT).show()
        clearState()
        setUpUi()
    }

    fun onAddSurveySuccess(surveyModel: SurveyModel){
        loadingDialog.dismiss()
        LoginActivity.roomDb.saveSurvey(surveyModel)
        Toast.makeText(requireContext(), "posted!", Toast.LENGTH_SHORT).show()
    }

    fun onAddSurveyFailed(error: String){
        loadingDialog.dismiss()
        Toast.makeText(requireContext(), Utilities.getException(error), Toast.LENGTH_SHORT).show()
    }

    private fun checkSpell(){
        val spellChecker = SpellChecker(this)
        spellChecker.execute(binding.etAddQuestion.text.toString())
    }

    override fun doAfterSpellCheckerSuccess(text: String) {
        binding.etAddQuestion.setText(text)
    }

    private fun checkConnection(){
        MainActivity.changeMeToCheckConnection.value = MainActivity.changeMeToCheckConnection.value!! + 1
    }

    private fun showLoadingDialog() {
        loadingDialog = Dialog(requireContext())
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setCancelable(false)
        loadingDialog.setContentView(R.layout.loading_dialog)
        loadingDialog.show()
    }

    @SuppressLint("MissingPermission")
    private fun gettingLocation(): Pair<String, String> {
        var lat = ""
        var lng = ""
        val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (location != null) {
            lat =  location.latitude.toString()
            lng =  location.longitude.toString()
        }
        return Pair(lat, lng)
    }
}