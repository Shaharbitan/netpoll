package com.example.netpoll.ui.profile

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.Utilities.Extensions.toSurveyModel
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.databinding.FragmentProfileBinding
import com.example.netpoll.models.ProfileModel
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.adapters.ProfileSurveyAdapter
import com.example.netpoll.ui.feed.FeedFragment
import com.example.netpoll.ui.login.LoginActivity
import com.example.netpoll.ui.profile.ProfileViewModel
import kotlinx.android.synthetic.main.edit_profile_dialog.*
import java.util.*

class ProfileFragment : Fragment() {

    companion object {
        lateinit var surveysAdapter: ProfileSurveyAdapter
        lateinit var profileViewModel: ProfileViewModel
        val surveysList = ArrayList<SurveyModel>()
    }
    private lateinit var binding: FragmentProfileBinding
    private lateinit var surveysRecyclerView : RecyclerView
    private lateinit var loadingDialog: Dialog
    private var newProfilePicture = ""
    private var newUserName = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        profileViewModel = ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java)
        binding.profileViewModel = profileViewModel
        surveysAdapter = ProfileSurveyAdapter(surveysList, requireContext())
        this.surveysRecyclerView = binding.rvProfileSurveys
        this.surveysRecyclerView.adapter = surveysAdapter
        this.surveysRecyclerView.layoutManager = LinearLayoutManager(context)
        loadData()
        setUpListener()
        return binding.root
    }

    private fun loadData(){
        val connectToNetwork = MainActivity.connectToNetwork
        binding.ivProfileEdit.isEnabled = connectToNetwork
        if (connectToNetwork)
            getProfile()
        else
            setUiFromRoom()
    }

    private fun getProfile(){
        val picture = binding.profileViewModel!!.profilePic.value!!
        val username = binding.profileViewModel!!.username.value!!
        if (username == "" || picture == "")
        {
            showLoadingDialog()
            FirebaseStore.getProfileById(LoginActivity.auth.currentUser!!.uid, this)
        }
        else
           setUi(picture, username)
    }

    private fun getSurveys(){
        FirebaseStore.setProfileSurveyListener(this, LoginActivity.auth.currentUser!!.uid)
    }

    fun setUiWithFireStore(profileModel: ProfileModel){
        setUi(profileModel.profilePic, profileModel.username)
        getSurveys()
        loadingDialog.dismiss()
    }

     private fun setUiFromRoom(){
        val profile = LoginActivity.roomDb.getProfileByUserId(LoginActivity.auth.currentUser!!.uid)
        setUpSurveysFromRoom()
        setUi(profile.profilePic, profile.username)
    }

    fun performClick(){
        MainActivity.navController.navigate(R.id.profileFragment)
    }

    private fun setUi(picture: String, username: String){
        binding.tvProfileUsername.text = username
        binding.ivProfileProfilePic.setImageBitmap(Utilities.convertStringToBitmap(picture))
        binding.profileViewModel!!.profilePic.value = picture
        binding.profileViewModel!!.username.value = username
    }

    private fun showLoadingDialog() {
        loadingDialog = Dialog(requireContext())
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setCancelable(false)
        loadingDialog.setContentView(R.layout.loading_dialog)
        loadingDialog.show()
    }

     private fun showEditProfileDialog(first: Boolean) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.edit_profile_dialog)
        if (first) {
            newUserName = binding.profileViewModel!!.username.value!!
            newProfilePicture = binding.profileViewModel!!.profilePic.value!!
        }
        dialog.et_edit_profile_username.setText(newUserName)
        dialog.iv_edit_profile_picture.setImageBitmap(Utilities.convertStringToBitmap(newProfilePicture))

        dialog.iv_edit_profile_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.iv_edit_profile_save.setOnClickListener {
            val username = dialog.et_edit_profile_username.text.toString()
            if (username != "") {
                updateProfileInFireStore(username, newProfilePicture)
                updateProfileInRoom(username, newProfilePicture)
                dialog.dismiss()
                binding.profileViewModel!!.username.value = ""
                binding.profileViewModel!!.profilePic.value = ""
            }
        }

        dialog.iv_edit_profile_picture.setOnClickListener {
            newUserName = dialog.et_edit_profile_username.text.toString()
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, 100)
            dialog.dismiss()
        }

        dialog.show()
    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == 100) {
            val profilePicUri = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(
                requireContext().contentResolver,
                profilePicUri
            )
            newProfilePicture = Utilities.convertBitmapToString(bitmap)
            showEditProfileDialog(false)
        }
    }

    private fun setUpListener(){
        binding.ivProfileEdit.setOnClickListener {
            showEditProfileDialog(true)
        }
        binding.ivProfileLogout.setOnClickListener {
            LoginActivity.auth.signOut()
            surveysList.clear()
            surveysAdapter.notifyDataSetChanged()
            FeedFragment.surveysList.clear()
            FeedFragment.surveysAdapter.notifyDataSetChanged()
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            requireActivity().finish()
        }
    }
    
     private fun updateProfileInFireStore(username: String, profilePicture: String){
        FirebaseStore.updateProfileById(LoginActivity.auth.uid!!, username, profilePicture, this)
    }

    private fun updateProfileInRoom(username: String, profilePicture: String){
        LoginActivity.roomDb.updateProfileById(username, profilePicture)
    }

    private fun setUpSurveysFromRoom(){
        val surveys = LoginActivity.roomDb.getSurveysByUserId(LoginActivity.auth.uid!!)
        surveysList.clear()
        for (survey in surveys) {
            surveysList.add(survey.toSurveyModel())
        }
        surveysAdapter.notifyDataSetChanged()
    }
}
