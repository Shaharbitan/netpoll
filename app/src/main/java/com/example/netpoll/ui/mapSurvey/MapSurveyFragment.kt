package com.example.netpoll.ui.mapSurvey

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TableRow
import androidx.databinding.DataBindingUtil
import com.example.netpoll.R
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.databinding.FragmentMapSurveyBinding
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.adapters.ProfileSurveyAdapter
import kotlinx.android.synthetic.main.map_survey_item.*
import kotlinx.android.synthetic.main.survey_item.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import java.util.*

class MapSurveyFragment : Fragment() {
    private lateinit var binding: FragmentMapSurveyBinding
    private lateinit var mapView: MapView
    private lateinit var loadingDialog: Dialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map_survey, container, false);
        mapView = binding.mapView
        Configuration.getInstance().userAgentValue = "test"
        getSurveys()
        return binding.root
    }

    private fun getSurveys(){
        showLoadingDialog()
        FirebaseStore.getSurveys(this)
    }

    private fun initMap(surveys: ArrayList<SurveyModel>){
        if (surveys.size > 0){
            mapView.setTileSource(TileSourceFactory.MAPNIK)
            mapView.setBuiltInZoomControls(true)
            mapView.setMultiTouchControls(true)

            val startPoint = GeoPoint(31.969896, 34.772101)
            mapView.controller.setCenter(startPoint)
            mapView.controller.setZoom(4.0)

            for (survey in surveys) {
                val marker = Marker(mapView)
                marker.position = GeoPoint(survey.lat.toDouble(), survey.lng.toDouble())
                marker.title = survey.question
                marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                marker.setOnMarkerClickListener { marker, mapView ->
                    surveyDialog(survey)
                    true
                }
                mapView.overlays.add(marker)
            }
        }

    }

    fun onGetSurveys(surveys: ArrayList<SurveyModel>){
        loadingDialog.dismiss()
        initMap(surveys)
    }

    private fun showLoadingDialog() {
        loadingDialog = Dialog(requireContext())
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setCancelable(false)
        loadingDialog.setContentView(R.layout.loading_dialog)
        loadingDialog.show()
    }

    private fun surveyDialog(surveyModel: SurveyModel){
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.map_survey_item)

        dialog.iv_map_profile_picture.setImageBitmap(Utilities.convertStringToBitmap(surveyModel.profilePicture))
        dialog.tv_map_username.text = surveyModel.username
        dialog.tv_map_question.text = surveyModel.question
        setAnswers(dialog.map_survey_answers, surveyModel)

        dialog.iv_map_survey_picture.setImageBitmap(Utilities.convertStringToBitmap(surveyModel.picture))

        dialog.iv_map_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun setAnswers(answers: LinearLayout, survey: SurveyModel){
        if (answers.childCount == 0) {
            val table = TableLayout(context)
            for (answer in survey.answers!!.keys) {
                val row = TableRow(context)
                val answerCell = MapCell(requireContext(), "$answer   ", true)
                val valueCell = MapCell(requireContext(), " " + survey.answers!![answer].toString() + " ", false)
                row.addView(answerCell)
                row.addView(valueCell)
                table.addView(row)
            }
            answers.addView(table)
        }
    }

    @SuppressLint("ViewConstructor")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    class MapCell(context: Context, text: String, isAnswer: Boolean): androidx.appcompat.widget.AppCompatTextView(context){
        init {
            this.text = text
            this.setTypeface(Typeface.DEFAULT_BOLD)
            if (isAnswer) {
                this.background = context.getDrawable(R.drawable.cell)
                this.width = context.resources.getDimension(R.dimen.map_survey_answer).toInt()
                this.isVerticalScrollBarEnabled = true
            }
        }
    }
}