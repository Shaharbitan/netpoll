package com.example.netpoll.ui.editSurvey

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.Utilities.Utilities
import com.example.netpoll.databinding.FragmentEditSurveyBinding
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.adapters.ProfileSurveyAdapter
import com.example.netpoll.ui.login.LoginActivity

class EditSurveyFragment : Fragment() {
        private lateinit var binding: FragmentEditSurveyBinding
        private lateinit var args: EditSurveyFragmentArgs
        private var newPicture = ""
        private var oldQuestion = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_survey, container, false);
        args = EditSurveyFragmentArgs.fromBundle(requireArguments())
        setUi()
        setUpListeners()
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        MainActivity.bottomNavigationMenu.visibility = View.VISIBLE
    }

    override fun onStop() {
        super.onStop()
        MainActivity.bottomNavigationMenu.visibility = View.VISIBLE
    }

    private fun setUi(){
        if (args.question != "")
            oldQuestion = args.question
        if (ProfileSurveyAdapter.pictureAction != "")
            newPicture = ProfileSurveyAdapter.pictureAction
            binding.ivEditSurveyPicture.setImageBitmap(Utilities.convertStringToBitmap(newPicture))
        MainActivity.bottomNavigationMenu.visibility = View.INVISIBLE
        binding.etEditSurveyQuestion.setText(args.question)
    }

    private fun setUpListeners(){
        binding.ivEditCanncel.setOnClickListener {
            MainActivity.navController.navigate(R.id.profileFragment)
        }
        binding.ivEditSurveyPicture.setOnClickListener {
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, 100)
        }
        binding.ivEditSave.setOnClickListener {
            updateSurveyInFireStore()
            MainActivity.navController.navigate(R.id.profileFragment)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == 100) {
            val profilePicUri = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(
                requireContext().contentResolver,
                profilePicUri
            )
            newPicture = Utilities.convertBitmapToString(bitmap)
            binding.ivEditSurveyPicture.setImageURI(profilePicUri)
        }
    }

    private fun updateSurveyInFireStore(){
        val newQuestion = binding.etEditSurveyQuestion.text.toString()
        if (newQuestion != ""){
            FirebaseStore.updateSurveyByQuestion(oldQuestion, newQuestion, newPicture)
            LoginActivity.roomDb.updateSurveyByIdAndQuestion(oldQuestion, newQuestion, newPicture)
        }
    }
}