package com.example.netpoll.ui.feed

import android.app.Dialog
import android.content.ClipData
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.netpoll.MainActivity
import com.example.netpoll.R
import com.example.netpoll.databinding.FragmentFeedBinding
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.adapters.FeedSurveyAdapter
import com.example.netpoll.ui.login.LoginActivity
import java.util.*

class FeedFragment : Fragment() {
    companion object {
        lateinit var surveysAdapter: FeedSurveyAdapter
        val surveysList = ArrayList<SurveyModel>()
    }

    private lateinit var binding: FragmentFeedBinding
    private lateinit var surveysRecyclerView : RecyclerView
    private lateinit var loadingDialog: Dialog



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed, container, false);
        surveysAdapter = FeedSurveyAdapter(surveysList, requireContext())
        this.surveysRecyclerView = binding.rvFeedSurvey
        this.surveysRecyclerView.adapter = surveysAdapter
        this.surveysRecyclerView.layoutManager = LinearLayoutManager(context)
        checkConnection()
        getSurveys()
        return binding.root
    }

    private fun getSurveys(){
        showLoadingDialog()
        FirebaseStore.setFeedSurveyListener(this)
    }

    private fun showLoadingDialog() {
        loadingDialog = Dialog(requireContext())
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setCancelable(false)
        loadingDialog.setContentView(R.layout.loading_dialog)
        loadingDialog.show()
    }

    fun dismissDialog(){
        loadingDialog.dismiss()
    }
    private fun checkConnection(){
        MainActivity.changeMeToCheckConnection.value = MainActivity.changeMeToCheckConnection.value!! + 1
    }
}