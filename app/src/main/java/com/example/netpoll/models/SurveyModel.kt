package com.example.netpoll.models

import java.util.*

data class SurveyModel(var userId: String,
                       var username: String,
                       var profilePicture: String,
                       var question: String,
                       var answers: HashMap<String, Int>?,
                       var picture: String,
                       var peoplesAnswers: HashMap<String, String>,
                       var lat: String,
                       var lng: String) {

}