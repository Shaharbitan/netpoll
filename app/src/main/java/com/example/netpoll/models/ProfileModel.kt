package com.example.netpoll.models

import android.net.Uri

data class ProfileModel(var username: String, var userId:String, var email: String, var password:String, var profilePic: String) {
}