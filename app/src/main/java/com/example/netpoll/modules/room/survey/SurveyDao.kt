package com.example.netpoll.modules.room.survey

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import java.util.*

@Dao
interface SurveyDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveSurvey(survey: SurveyEntity)

    @Query(value = "UPDATE SurveyEntity SET question = :surveyNewQuestion, picture= :newPicture where userId = :profileUserId AND question = :surveyOldQuestion")
    fun updateSurveyByUserIdAndQuestion(profileUserId: String, surveyOldQuestion: String, surveyNewQuestion: String, newPicture: String)

    @Query (value = "Select * from SurveyEntity where userId = :profileUserId")
    fun getSurveysByUserId(profileUserId: String) : List<SurveyEntity>

    @Query(value = "UPDATE SurveyEntity SET answers = :newAnswers where userId = :profileUserId AND question = :surveyQuestion")
    fun updateSurveyAnswersNyProfileId(profileUserId: String, surveyQuestion: String, newAnswers: String)
}