package com.example.netpoll.modules.room

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.example.netpoll.Utilities.Extensions.toEntity
import com.example.netpoll.models.ProfileModel
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.room.profile.ProfileEntity
import com.example.netpoll.modules.room.survey.SurveyEntity
import com.example.netpoll.ui.login.LoginActivity
import java.util.*

class RoomManager(context: Context) {

    private val roomDb= Room.databaseBuilder(context, AppDb::class.java,"AppDb").build()

    fun saveProfile(profile: ProfileModel){
        Thread(Runnable {
            try {
                roomDb.profileDao().saveProfile(profile.toEntity())
            } catch (e: Exception) {
                Log.e("shamla", "saveProfile: ${e.message}")
            }
        }).start()
    }

    fun saveSurvey(survey: SurveyModel){
        Thread(Runnable {
            roomDb.surveyDao().saveSurvey(survey.toEntity())
        }).start()
    }

    fun getProfileByUserId(profileUserId: String): ProfileEntity {
        var profile = ProfileEntity()
        val thread = Thread(Runnable {
            profile = roomDb.profileDao().getProfileByUserId(profileUserId)
        })
        thread.start()
        thread.join()
        return profile
    }

    fun getSurveysByUserId(profileUserId: String): ArrayList<SurveyEntity> {
        var surveys  = ArrayList<SurveyEntity>()
        val thread = Thread(Runnable {
            val resultSurveys = roomDb.surveyDao().getSurveysByUserId(profileUserId)
            surveys = ArrayList(resultSurveys as List<SurveyEntity>)
        })
        thread.start()
        thread.join()
        return surveys
    }

    fun updateSurveyByIdAndQuestion(oldQuestion: String, newQuestion: String, picture: String){
        Thread(Runnable {
            roomDb.surveyDao().updateSurveyByUserIdAndQuestion(LoginActivity.auth.uid!!, oldQuestion, newQuestion, picture)
        }).start()
    }

    fun updateSurveyAnswersNyProfileId(question: String, answers: String){
        Thread(Runnable {
            roomDb.surveyDao().updateSurveyAnswersNyProfileId(LoginActivity.auth.uid!!, question, answers)
        }).start()
    }

    fun updateProfileById(newUsername: String, picture: String){
        Thread(Runnable {
            roomDb.profileDao().updateProfileByUserId(LoginActivity.auth.uid!!, newUsername, picture)
        }).start()
    }
}