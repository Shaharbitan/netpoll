package com.example.netpoll.modules.spellChecker

import android.os.AsyncTask
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request


class SpellChecker(private val listener: SpellCheckerListener): AsyncTask<String, String, String>() {

    override fun doInBackground(vararg params: String?): String? {
        val stringToCheck = params[0]
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/spelling/SpellCheck?text=$stringToCheck")
            .get()
            .addHeader("X-RapidAPI-Key", "9e93201374msh659216c1a433555p1a070ejsnf452d7ce1951")
            .addHeader("X-RapidAPI-Host", "contextualwebsearch-websearch-v1.p.rapidapi.com")
            .build()

        return client.newCall(request).execute().body().string()
    }

    override fun onPostExecute(result: String?) {
        listener.doAfterSpellCheckerSuccess(result!!)
    }
}