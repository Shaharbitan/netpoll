package com.example.netpoll.modules.room.profile

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class ProfileEntity {

    @PrimaryKey
    var profileId : Int = 0

    @ColumnInfo (name="username")
    var username: String = ""

    @ColumnInfo (name="userId")
    var userId: String = ""

    @ColumnInfo (name="email")
    var email: String =""

    @ColumnInfo (name="password")
    var password:String =""

    @ColumnInfo (name="profilePic")
    var profilePic: String =""
}