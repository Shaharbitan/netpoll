package com.example.netpoll.modules.firebaseStore

import android.util.Log
import com.example.netpoll.Utilities.Extensions.toDocument
import com.example.netpoll.Utilities.Extensions.toJsonString
import com.example.netpoll.Utilities.Extensions.toProfileModel
import com.example.netpoll.Utilities.Extensions.toSurveyModel
import com.example.netpoll.models.ProfileModel
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.ui.addSurvey.AddSurveyFragment
import com.example.netpoll.ui.feed.FeedFragment
import com.example.netpoll.ui.login.LoginActivity
import com.example.netpoll.ui.mapSurvey.MapSurveyFragment
import com.example.netpoll.ui.profile.ProfileFragment
import com.example.netpoll.ui.signUp.SignUpActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import java.util.*


class FirebaseStore {
    companion object {
        private var fireStoreDb = FirebaseFirestore.getInstance()

        fun addProfile(profile: ProfileModel, signUpActivity: SignUpActivity){
            fireStoreDb.collection("users")
                .add(profile.toDocument())
                .addOnSuccessListener(OnSuccessListener<DocumentReference> { documentReference ->
                    signUpActivity.signUpSuccess(profile)
                    Log.d("TAG", "DocumentSnapshot added with ID: " + documentReference.id)
                })
                .addOnFailureListener(OnFailureListener { e ->
                    Log.w("TAG", "Error adding document", e)
                    signUpActivity.signUpFailed(e.message.toString())
                })
        }

        fun addSurvey(survey: SurveyModel, addSurveyFragment: AddSurveyFragment){
            fireStoreDb.collection("surveys")
                .add(survey.toDocument())
                .addOnSuccessListener(OnSuccessListener<DocumentReference> { documentReference ->
                    Log.d("TAG", "DocumentSnapshot added with ID: " + documentReference.id)
                    addSurveyFragment.onAddSurveySuccess(survey)
                })
                .addOnFailureListener(OnFailureListener { e ->
                    Log.w("TAG", "Error adding document", e)
                    addSurveyFragment.onAddSurveyFailed(e.message.toString())
                })
        }

        fun getProfileById(id: String, profileFragment: ProfileFragment) {
            fireStoreDb.collection("users")
                .get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                    for (document in task.result) {
                        if (document.data.get("userId") == id) {
                            val profile = document.data as HashMap<String, Any>
                            profileFragment.setUiWithFireStore(profile.toProfileModel())
                        } }
                    } else {
                        Log.e("TAG", "Error getting documents.", task.exception)
                    }
                })
        }

        fun updateProfileById(id: String, newUserName: String, newPicture: String, profileFragment: ProfileFragment) {
            fireStoreDb.collection("users")
                .get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        for (document in task.result) {
                            if (document.data.get("userId") == id) {
                                val profile = document.data as HashMap<String, Any>
                                val profileModel = profile.toProfileModel()
                                profileModel.username = newUserName
                                profileModel.profilePic = newPicture
                                fireStoreDb.collection("users").document(document.id).update(profileModel.toDocument());
                                profileFragment.performClick()
                            } }
                    } else {
                        Log.e("TAG", "Error getting documents.", task.exception)
                    }
                })
        }

        fun updateSurveyByQuestion(oldQuestion: String, newQuestion: String, picture: String) {
            fireStoreDb.collection("surveys")
                .get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        for (document in task.result) {
                            val survey = document.data as HashMap<String, Any>
                            val surveyModel = survey.toSurveyModel()
                            if (surveyModel.userId == LoginActivity.auth.uid) {
                                if (surveyModel.question == oldQuestion) {
                                    surveyModel.question = newQuestion
                                    surveyModel.picture = picture
                                    fireStoreDb.collection("surveys").document(document.id).update(surveyModel.toDocument());
                                    break
                                }
                            }
                        }
                    } else {
                        Log.e("TAG", "Error getting documents.", task.exception)
                    }
                })
        }

        fun updateSurveyAnswer(question: String, answerChecked: String, answerUnchecked: String, action: Int) {
            fireStoreDb.collection("surveys")
                .get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        for (document in task.result) {
                            val survey = document.data as HashMap<String, Any>
                            val surveyModel = survey.toSurveyModel()
                            if (surveyModel.userId != LoginActivity.auth.uid && surveyModel.question == question) {
                                var index = 0
                                val newAnswers = HashMap<String, Int>()
                                val answers = ArrayList(surveyModel.answers!!.keys)
                                for (value: Number in surveyModel.answers!!.values) {
                                    newAnswers.put(answers[index++], value.toInt())
                                }
                                val valueToChange = newAnswers.get(answerChecked)
                                newAnswers.put(answerChecked, (valueToChange!! + (1 * action)))
                                if (answerUnchecked != ""){
                                    val valueToDecrease = newAnswers.get(answerUnchecked)!! - 1
                                    newAnswers.put(answerUnchecked, valueToDecrease)
                                }
                                surveyModel.answers = newAnswers
                                if (action == -1)
                                    surveyModel.peoplesAnswers.remove(LoginActivity.auth.uid!!)
                                else
                                    surveyModel.peoplesAnswers.put(LoginActivity.auth.uid!!, answerChecked)

                                fireStoreDb.collection("surveys").document(document.id)
                                    .update(surveyModel.toDocument());
                                break
                            }
                        }
                    } else {
                        Log.e("TAG", "Error getting documents.", task.exception)
                    }
                })
        }

        fun setProfileSurveyListener(profileFragment: ProfileFragment, id: String) {
            fireStoreDb.collection("surveys").addSnapshotListener{
                value , e ->
                ProfileFragment.surveysList.clear()
                for (document in value.documents) {
                    val survey = document.data as HashMap<String, Any>
                    val surveyModel = survey.toSurveyModel()
                    if (surveyModel.userId == id) {
                        LoginActivity.roomDb.updateSurveyAnswersNyProfileId(surveyModel.question, surveyModel.answers!!.toJsonString())
                        surveyModel.username = ProfileFragment.profileViewModel.username.value!!
                        surveyModel.profilePicture = ProfileFragment.profileViewModel.profilePic.value!!
                        ProfileFragment.surveysList.add(surveyModel)
                    }
                }
                ProfileFragment.surveysAdapter.notifyDataSetChanged()
            }
        }

        fun setFeedSurveyListener(feedFragment: FeedFragment) {
            val users = HashMap<String, ProfileModel>()
            fireStoreDb.collection("surveys").addSnapshotListener{
                    value , e ->
                fireStoreDb.collection("users")
                    .get()
                    .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                        if (task.isSuccessful) {
                            for (profileDocument in task.result) {
                                val profile = profileDocument.data as HashMap<String, Any>
                                val profileModel = profile.toProfileModel()
                                users.put(profileModel.userId, profileModel)
                            }
                            FeedFragment.surveysList.clear()
                            for (document in value.documents) {
                                val survey = document.data as HashMap<String, Any>
                                val surveyModel = survey.toSurveyModel()
                                if (surveyModel.userId != LoginActivity.auth.uid!!) {
                                    surveyModel.username = users[surveyModel.userId]!!.username
                                    surveyModel.profilePicture = users[surveyModel.userId]!!.profilePic
                                    FeedFragment.surveysList.add(surveyModel)
                                }
                            }
                            FeedFragment.surveysAdapter.notifyDataSetChanged()
                            feedFragment.dismissDialog()
                        } else {
                            Log.e("TAG", "Error getting documents.", task.exception)
                        }
                    })
            }
        }

        fun getSurveys(mapSurveyFragment: MapSurveyFragment) {
            val users = HashMap<String, ProfileModel>()
            val surveysList = ArrayList<SurveyModel>()
            fireStoreDb.collection("surveys").get()
                .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { surveyTask ->
                    if (surveyTask.isSuccessful) {
                        fireStoreDb.collection("users")
                            .get()
                            .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                                if (task.isSuccessful) {
                                    for (profileDocument in task.result) {
                                        val profile = profileDocument.data as HashMap<String, Any>
                                        val profileModel = profile.toProfileModel()
                                        users.put(profileModel.userId, profileModel)
                                    }
                                    for (surveyDocument in surveyTask.result) {
                                        val survey = surveyDocument.data as HashMap<String, Any>
                                        val surveyModel = survey.toSurveyModel()
                                        surveyModel.username = users[surveyModel.userId]!!.username
                                        surveyModel.profilePicture = users[surveyModel.userId]!!.profilePic
                                        surveysList.add(surveyModel)
                                    }
                                    mapSurveyFragment.onGetSurveys(surveysList)
                                }else { Log.e("TAG", "Error getting documents.", task.exception) }
                            })
                    } else {
                        Log.e("TAG", "Error getting documents.", surveyTask.exception)
                    }
                })
        }
    }
}