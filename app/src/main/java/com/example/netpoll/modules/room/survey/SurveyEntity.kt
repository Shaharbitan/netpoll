package com.example.netpoll.modules.room.survey

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
class SurveyEntity {

    @PrimaryKey
    var surveyId : Int = 0

    @ColumnInfo(name="userId")
    var userId: String = ""

    @ColumnInfo(name="username")
    var username: String = ""

    @ColumnInfo(name="profilePicture")
    var profilePicture: String = ""

    @ColumnInfo(name="question")
    var question: String = ""

    @ColumnInfo(name="answers")
    var answers: String = ""

    @ColumnInfo(name="picture")
    var picture: String =""

    @ColumnInfo(name="peoplesAnswers")
    var peoplesAnswers: String =""
}