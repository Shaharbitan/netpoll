package com.example.netpoll.modules.room.profile

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProfileDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveProfile(profile: ProfileEntity)

    @Query (value = "Select * from ProfileEntity where userId = :profileUserId")
    fun getProfileByUserId(profileUserId: String) : ProfileEntity

    @Query(value = "UPDATE ProfileEntity SET username = :newUsername, profilePic= :newPicture where userId = :profileUserId")
    fun updateProfileByUserId(profileUserId: String, newUsername: String, newPicture: String)
}