package com.example.netpoll.modules.spellChecker

interface SpellCheckerListener {
    fun doAfterSpellCheckerSuccess(text: String)
}