package com.example.netpoll.modules.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.netpoll.modules.room.profile.ProfileDao
import com.example.netpoll.modules.room.profile.ProfileEntity
import com.example.netpoll.modules.room.survey.SurveyDao
import com.example.netpoll.modules.room.survey.SurveyEntity

@Database (entities = [(ProfileEntity::class), (SurveyEntity::class)],version = 1)
abstract class AppDb : RoomDatabase(){
    abstract fun profileDao(): ProfileDao
    abstract fun surveyDao(): SurveyDao
}