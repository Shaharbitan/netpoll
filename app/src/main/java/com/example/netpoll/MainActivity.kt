package com.example.netpoll

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.netpoll.databinding.ActivityMainBinding
import com.example.netpoll.models.ProfileModel
import com.example.netpoll.modules.firebaseStore.FirebaseStore
import com.example.netpoll.ui.login.LoginActivity
import com.example.netpoll.ui.profile.ProfileViewModel
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var navController: NavController
        lateinit var bottomNavigationMenu: BottomNavigationView
        val changeMeToCheckConnection = MutableLiveData<Int>(0)
        var connectToNetwork = false
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        val view: View = binding.root
        setContentView(view)
        navController= Navigation.findNavController(this,R.id.activity_main_nav_host_fragment)
        bottomNavigationMenu = binding.bottomNavigationView
        findViewById<View>(R.id.feedMenuBtn).performClick()
        setUpObservers()
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            navigateToFragment(it.itemId)
            true
        }
        requestAppPermissions()
    }

    override fun onBackPressed() {

    }

    private fun setUpObservers(){
        changeMeToCheckConnection.observe(this){
            connectToNetwork = isOnline()
            val feedButton = findViewById<View>(R.id.feedMenuBtn)
            val addSurveyButton = findViewById<View>(R.id.AddMenuBtn)
            val profileButton = findViewById<View>(R.id.profileMenuBtn)
            val mapButton = findViewById<View>(R.id.mapMenuBtn)


            if (!connectToNetwork)
                profileButton.performClick()

            feedButton.isClickable = connectToNetwork
            addSurveyButton.isClickable = connectToNetwork
            mapButton.isClickable = connectToNetwork
        }
    }

    private fun navigateToFragment(itemId: Int){
        when(itemId){
            R.id.profileMenuBtn -> {
                navController.navigate(R.id.profileFragment)
            }
            R.id.feedMenuBtn -> {
                navController.navigate(R.id.feedFragment)
            }
            R.id.AddMenuBtn -> {
                navController.navigate(R.id.addSurveyFragment)
            }
            R.id.mapMenuBtn -> {
                navController.navigate(R.id.mapSurveyFragment)
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun isOnline(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    private fun requestAppPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        }
    }
}