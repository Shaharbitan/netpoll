package com.example.netpoll.Utilities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.RectF
import android.util.Base64
import java.io.ByteArrayOutputStream


class Utilities {
    companion object {
        fun convertBitmapToString(bitmap: Bitmap): String {
            val croppedBitmap = scaleCenterCrop(bitmap)
            val baos = ByteArrayOutputStream()
            croppedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data: ByteArray = baos.toByteArray()
            return Base64.encodeToString(data, Base64.DEFAULT)
        }

        fun convertStringToBitmap(string: String): Bitmap? {
            val imageBytes = Base64.decode(string, 0)
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }

        fun getException(exception: String): String{
            return exception.split(":").last()
        }

        private  fun scaleCenterCrop(source: Bitmap): Bitmap? {
            val sourceWidth = source.width
            val sourceHeight = source.height

            val xScale = 500.toFloat() / sourceWidth
            val yScale = 500.toFloat() / sourceHeight
            val scale = Math.max(xScale, yScale)

            val scaledWidth = scale * sourceWidth
            val scaledHeight = scale * sourceHeight

            val left = (500 - scaledWidth) / 2
            val top = (500 - scaledHeight) / 2

            val targetRect = RectF(left, top, left + scaledWidth, top + scaledHeight)

            val dest = Bitmap.createBitmap(500, 500, source.config)
            val canvas = Canvas(dest)
            canvas.drawBitmap(source, null, targetRect, null)
            return dest
        }
    }
}