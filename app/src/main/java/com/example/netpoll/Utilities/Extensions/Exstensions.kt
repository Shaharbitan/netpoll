package com.example.netpoll.Utilities.Extensions

import com.example.netpoll.models.ProfileModel
import com.example.netpoll.models.SurveyModel
import com.example.netpoll.modules.room.profile.ProfileEntity
import com.example.netpoll.modules.room.survey.SurveyEntity
import com.example.netpoll.ui.addSurvey.answer.Answer
import java.util.*

fun String.validate () : Boolean {
    return  (this.length > 0)
}

fun ProfileModel.toDocument(): HashMap<String, Any>{
    var user : HashMap<String, Any> = HashMap<String, Any> ()
    user.put("username", this.username)
    user.put("userId", this.userId)
    user.put("email", this.email)
    user.put("password", this.password)
    user.put("profilePic", this.profilePic)
    return user
}

fun HashMap<String, Any>.toProfileModel(): ProfileModel{
    return ProfileModel(this.get("username") as String,
        this.get("userId") as String, this.get("email") as String,
        this.get("password") as String, this.get("profilePic") as String
    )
}

fun HashMap<String, Any>.toSurveyModel(): SurveyModel{
    return SurveyModel(this.get("userId") as String,
        this.get("username") as String, this.get("profilePicture") as String,
        this.get("question") as String, this.get("answers") as HashMap<String, Int>,
        this.get("picture") as String,
        this.get("peoplesAnswers") as HashMap<String, String>,
        this.get("lat") as String,
        this.get("lng") as String)
}

fun ProfileModel.toEntity(): ProfileEntity{
    val profileEntity = ProfileEntity()
    profileEntity.username= this.username
    profileEntity.userId= this.userId
    profileEntity.email= this.email
    profileEntity.password= this.password
    profileEntity.profilePic= this.profilePic
    return profileEntity
}

fun SurveyModel.toDocument(): HashMap<String, Any>{
    var survey : HashMap<String, Any> = HashMap<String, Any> ()
    survey.put("userId", this.userId)
    survey.put("username", this.username)
    survey.put("profilePicture", this.profilePicture)
    survey.put("question", this.question)
    survey.put("answers", this.answers!!)
    survey.put("picture", this.picture)
    survey.put("peoplesAnswers", this.peoplesAnswers)
    survey.put("lat", this.lat)
    survey.put("lng", this.lng)
    return survey
}

fun SurveyModel.toEntity(): SurveyEntity{
    val surveyEntity = SurveyEntity()
    surveyEntity.userId= this.userId
    surveyEntity.username = this.username
    surveyEntity.profilePicture = this.profilePicture
    surveyEntity.question= this.question
    surveyEntity.answers= this.answers!!.toJsonString()
    surveyEntity.picture= this.picture
    surveyEntity.peoplesAnswers = this.peoplesAnswers.toJson()
    return surveyEntity
}

fun SurveyEntity.toSurveyModel(): SurveyModel{
    return SurveyModel(this.userId,
        this.username,
        this.profilePicture,
        this.question,
        this.answers.toHashMap(),
        this.picture,
        HashMap<String, String>(), "", ""
    )
}

fun SurveyModel.validate(): Boolean {
     return (this.question != "" && this.answers != null && this.lat != "" && this.lng != "")
}

fun MutableList<Answer>.toHashMap(): HashMap<String, Int>? {
    val answers = HashMap<String, Int>()
    for (answer in this){
        answers.put(answer.text.toString(), 0)
        if (answer.text.toString().replace(" ", "") == ""){
            return null
        }
    }
    return answers
}

fun HashMap<String, Int>.toJsonString(): String{
    var jsonString = ""
    for (answer in this.keys){
        jsonString += "@$answer@${this[answer]}"
    }
    return jsonString
}

fun HashMap<String, String>.toJson(): String{
    var jsonString = ""
    for (answer in this.keys){
        jsonString += "@$answer@${this[answer]}"
    }
    return jsonString
}

fun String.toHashMap(): HashMap<String, Int> {
    val answers = HashMap<String, Int>()
    val answersString = this.split("@")
    var index = 1
    while (index < answersString.size) {
        answers.put(answersString[index], answersString[index + 1].toInt())
        index += 2
    }

    return answers
}